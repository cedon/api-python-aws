#!/bin/bash

#comment test
# comentario de teste
sleep 2

RED="\033[1;31m"
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

echo -e "${RED}""INICIO DA DEMONSTRACAO ${NOCOLOR}"

RESULTADO=`curl --silent /dev/null -H "Content-Type: application/json" -X GET http://localhost:8000/api/sum?term_one=3&term_two=2`
RESPOSTA='{
  "result": 5
}'

if [ "$RESULTADO" == "$RESPOSTA" ]
	then echo -e "${GREEN}SOMA OK!${NOCOLOR}"
else 
	echo -e "${RED}""RESULTADO INVÁLIDO ${NOCOLOR}"
fi


RESULTADO1=`curl --silent -H "Content-Type: application/json" -X GET http://localhost:8000/api/sub?term_one=3&term_two=2`
RESPOSTA1='{
  "result": 1
}'

if [ "$RESULTADO1" == "$RESPOSTA1" ]
        then echo -e "${GREEN}SUBTRAÇÃO OK!${NOCOLOR}"
else 
	echo -e "${RED}RESULTADO INVÁLIDO ${NOCOLOR}"
fi


RESULTADO2=`curl --silent -H "Content-Type: application/json" -X GET http://localhost:8000/api/mul?term_one=3&term_two=2`
RESPOSTA2='{
  "result": 6
}'

if [ "$RESULTADO2" == "$RESPOSTA2" ]
        then echo -e "${GREEN}MULTIPLICAÇÃO OK!${NOCOLOR}"
else 
	echo -e "${RED}RESULTADO INVÁLIDO ${NOCOLOR}"
fi


RESULTADO3=`curl --silent -H "Content-Type: application/json" -X GET http://localhost:8000/api/div?term_one=6&term_two=2`
RESPOSTA3='{
  "result": 3
}'

if [ "$RESULTADO3" == "$RESPOSTA3" ]
        then echo -e "${GREEN}DIVISÃO OK ${NOCOLOR}"
else 
	echo -e "${RED}RESULTADO INVÁLIDO ${NOCOLOR}"
fi
