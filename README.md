# API[terraform-aws-ansible-docker-python-flask]: 4 Operações Básicas

## About

    - Repositório com os pacotes de API que executa as 4 operações básicas
    -  A arquitetura prevë 3 ambientes: User Host, Gitlab e Instancia AWS-EC2
    -- User Host: ambiente do usuário onde são executados os comandos de provisionamento para a Instancia AWS-EC2
    -- Gitlab: ambiente de versionamento testes.
    -- Instancia AWS-EC2: ambiente de produção da API. Host hospedeiro da infra de containers docker.
    - A aplicação roda na porta 8000 e as função são acessadas pela pasta `endereço_host:8000/api/` 

## Usage

    - enviroment: "pacote de Deploy(ansible) da aplicação"
    - ec2site: "pacote das configurações(terraform) para criação do host EC2"
    - operacoes: "pacote com as classes que executam a API"
    - .gitlab-ci.yml: "arquivo de configuração CI"
    - Dockerfile: "arquivo com as configurações do container da aplicação. 
    - Instala dependencias e executa o run.py no container"
    - docker-compose.yml: "configurações da infraestrutura base do ambiente containerizado da aplicação"
    - requirements.txt: "arquivo com a descrição dos pacotes necessários para a execução da aplicação"
    - run.py: "instancia e inicia via python/flask o serviço da aplicação"
    - tests.sh: "executa os testes unitários nas funções utilizando o Curl"

## Requirements
Pacotes necessários para proisionamento e deploy

#### User host
    - Ansible 2.0+
    - Terraform 0.11+

#### AWS EC2 host   

    - ssh
    - sudo
    - python3
    - python3-apt
    - aptitude

## Provide AWS EC2 Instance
Caso não existir um host, entrar na pasta `ec2site` e rodar os comandos abaixo para gerar uma instancia EC2

    - acrescentar as chaves no arquivo variables.tf

`
    - $ terraform init
`

`
    - $ terraform plan 
`

`
    - $ terraform apply
`

## Deploy API Enviroment
O Deploy da aplicação é com ansible que prepara as dependencias, carrega os arquivos e levanta o docker-compose
no host EC2


*os comandos abaixo devem ser executados dentro da pasta `enviroment`.

    - acrescentar endereço do host EC2 no arquivo inventory.cfg
    - exemplo:

     [apioperacoes]

     192.168.100.11

    - para executar o comando abaixo é necessário configurar a chaves ssh de acesso à instancia EC2
    - tutorial para gerar e exportar as chaves ssh:  
    <https://ansible-tips-and-tricks.readthedocs.io/en/latest/ansible/commands/>

`
    - $ ansible-playbook -i inventory.cfg deploy.yml -u admin
`

    - o parametro '-u admin' pode ser substituido pelo usuario ssh configurado na instancia EC2(ex.:root,ubuntu)

## Aplicação web

A aplicação web consiste de uma API que executa as quatro operações básicas
da matemática: divisão, multiplicação, subtração, adição.

Existem 4 endpoints, 1 para cada operação:

Subtração:

`
GET /api/sub?term_one=<term_one>&term_two=<term_two>
`

Multiplicação:

`
GET /api/mul?term_one=<term_one>&term_two=<term_two>
`

Divisão:

`
GET /api/div?term_one=<term_one>&term_two=<term_two>
`

Adição:

`
GET /api/sum?term_one=<term_one>&term_two=<term_two>
`

Cada endpoint deve retornar um JSON contendo o resultado da operação na seguinte estrutura:

```
{'result': <int:result> }
```

Por exemplo:

`
GET /api/sub?term_one=4&term_two=1
`

deve retornar

```
{'result': 3}
```
