#!/usr/bin/env python3

class Subp():
    result = 0
    def __init__(self,n1: int, n2: int):
        if n1 >= n2:
            Subp.result = n1 - n2
        else:
            Subp.result = n2 - n1
