#!/usr/bin/env python3

class Divp():
    result = 0
    def __init__(self,n1: int, n2: int):
        if n1 >= n2:
            Divp.result = n1 / n2
        else:
            Divp.result = n2 /n1
