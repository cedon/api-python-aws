#!/usr/bin/env python3

import os
import markdown
from operacoes import sum, mul, div, sub
from flask import Flask, request, jsonify
#from flask_restful import Api

app = Flask(__name__)
#api = Api(app)

# read and display a markdown file(README.md) at host_address:8000
@app.route("/")
def index():
    with open(os.path.dirname(app.root_path) + '/' + 'README.md', 'r') as markdown_file:
        content = markdown_file.read()
        return markdown.markdown(content)

# functions somar,subtracao,multiplicacao and divisao recieving get requests term_one and term_two
# and return .json files
@app.route('/api/sum')
def somar():
    term_one = request.args.get('term_one', 1)
    term_two = request.args.get('term_two', 2)
    soma = sum.Sump(int(term_one),int(term_two))
    return jsonify({'result': soma.result})


@app.route('/api/sub')
def subtracao():
    term_one = request.args.get('term_one', 1)
    term_two = request.args.get('term_two', 2)
    diferenca = sub.Subp(int(term_one),int(term_two))
    return jsonify({'result': diferenca.result})


@app.route('/api/mul')
def multiplicacao():
    term_one = request.args.get('term_one', 1)
    term_two = request.args.get('term_two', 2)
    produto = mul.Mulp(int(term_one),int(term_two))
    return jsonify({'result': produto.result})


@app.route('/api/div')
def divisao():
    term_one = request.args.get('term_one', 1)
    term_two = request.args.get('term_two', 2)
    quociente = div.Divp(int(term_one),int(term_two))
    return jsonify({'result': int(quociente.result)})

